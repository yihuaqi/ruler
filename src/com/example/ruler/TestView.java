package com.example.ruler;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class TestView extends View{
	Paint mPaint;
	float start_x;
	float end_x;
	boolean measuring = false;
	public TestView(Context context) {
		super(context);
		mPaint = new Paint();
		mPaint.setColor(Color.BLACK);
		
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawLine(start_x, 0, start_x, 400, mPaint);
		if(measuring){
			canvas.drawLine(end_x, 0, end_x, 400, mPaint);
			
		}
		super.onDraw(canvas);
	}

	public void setStart_X(float event_x){
		start_x=event_x;
	}
	public void setEnd_X(float event_x){
		end_x=event_x;
	}
	public void startMeasuring(){
		measuring = true;
	}
	public void stopMeasuring(){
		measuring = false;
	}

}
