package com.example.ruler;

public class MeasureUtil {
	public static String KEY_REAL_DISTANCE = "real_distance";
	public static String SHARED_PREFERENCE_FILE = "distance_preference";
	public static int calibrate_Pix = 700;
	public static float calibrate_Distance = (float) 74.92;
	public static double pixToMillimeter(float pix){
		return Math.abs(pix*calibrate_Distance/calibrate_Pix);
	}
	public static void setRealDistance(float millimeter){
		calibrate_Distance = millimeter;
	}
	public static void setPixDistance(int pix){
		calibrate_Pix = pix;
	}
}
