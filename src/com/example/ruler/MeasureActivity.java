package com.example.ruler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



import android.app.Activity;

import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MeasureActivity extends Activity implements OnClickListener, SurfaceHolder.Callback,PreviewCallback{
	SurfaceView mSurfaceView;
	SurfaceHolder mSurfaceHolder;
	Camera mCamera;
	TextView distance_txt;
	TextView status_txt;
	Button back_btn;
	Button threshold_btn;
	ImageView bar_img;
	ImageView test_img;
	LinearLayout mLinearLayout;
	TestView mTestView;
	float start=0;
	float end=0;
	float current_x = 0;
	float prev_x = 0;
	long current_timeStamp=0;
	long prev_timeStamp=0;
	final float MEASURE_STEP = 0.0f;
	float takePicture_Threshold = 0.0f;
	double distance = 0.0;
	public static final int STATE_START=0;
	public static final int STATE_MEASURE=1;
	public static final int STATE_END=2;
	public static final float THRESHOLD_SPEED = 0.03f;
	int state = STATE_END;
	boolean usingThreshold = true;
	final String SAVING_PATH = "/RULER_DC/";
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.measure);
		findAllViews();
		
		File subdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+SAVING_PATH);
		if(!subdir.exists()){
			subdir.mkdir();
		}
	}

	private void findAllViews() {
		mTestView = new TestView(this);
		distance_txt = (TextView) findViewById(R.id.measure_distance_txt);
		status_txt = (TextView) findViewById(R.id.measure_status_txt);
		back_btn = (Button) findViewById(R.id.measure_back_btn);
		threshold_btn = (Button) findViewById(R.id.measure_threshold_btn);
		bar_img = (ImageView) findViewById(R.id.measure_bar_img);
		mLinearLayout = (LinearLayout) findViewById(R.id.measure_test_layout);
		mLinearLayout.addView(mTestView);
		back_btn.setOnClickListener(this);
		threshold_btn.setOnClickListener(this);
		mSurfaceView = (SurfaceView) findViewById(R.id.preview);
		mSurfaceHolder = mSurfaceView.getHolder();
		//mSurfaceHolder.addCallback(this);
		
		
	}

	@Override
	public void onClick(View v) {
		if(v==back_btn){
			finish();
		}
		if(v==threshold_btn){
			if(usingThreshold==true){
				usingThreshold = false;
				threshold_btn.setText("Threshold_Off");
			} else {
				usingThreshold = true;
				threshold_btn.setText("Threshold_On");
			}
		}
		
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(usingThreshold){
			onTouchEvent_UsingThreshold(event);
		} else {
			onTouchEvent_NotUsingThreshold(event);
		}

		return super.onTouchEvent(event);
	}

	private void onTouchEvent_NotUsingThreshold(MotionEvent event) {
		
		current_x = event.getX();
		current_timeStamp = System.currentTimeMillis();
		
		switch (event.getAction()) {
		
		case MotionEvent.ACTION_DOWN:
			mTestView.setStart_X(current_x);
			mTestView.stopMeasuring();
			status_txt.setText("Start:"+event.getX()+" "+event.getY());
			distance_txt.setText(0+"mm");
			start = event.getRawX();
			state=STATE_START;
			break;
			
		case MotionEvent.ACTION_UP:
			state = STATE_END;
			status_txt.setText("Action up:"+event.getX()+" "+event.getY());
			end = event.getX();
			distance = MeasureUtil.pixToMillimeter(end-start);
			distance_txt.setText(String.format("%6.2f", distance)+"mm");

			break;
		case MotionEvent.ACTION_MOVE:
			state = STATE_MEASURE;
			mTestView.setEnd_X(current_x);
			mTestView.startMeasuring();
			end = event.getX();
			distance = MeasureUtil.pixToMillimeter(end-start);
			distance_txt.setText(String.format("%6.2f", distance)+"mm");
			status_txt.setText("Action move:"+event.getX()+" "+event.getY());
			
			break;
			
		default:
			break;
		}
		mTestView.invalidate();
		prev_timeStamp = current_timeStamp;
		prev_x = current_x;
		
	}

	private void onTouchEvent_UsingThreshold(MotionEvent event) {
		
		current_x = event.getX();
		current_timeStamp = System.currentTimeMillis();
		
		switch (event.getAction()) {
		
		case MotionEvent.ACTION_DOWN:
			
			if(state==STATE_END){
				status_txt.setText("Touch down");
				distance_txt.setText(String.format("%6.2f", 0.0f)+"mm");
				start = 0;
				state=STATE_START;
				mTestView.stopMeasuring();
			}
			/*
			status_txt.setText("Start:"+event.getX()+" "+event.getY());
			distance_txt.setText(0+"mm");
			start = event.getRawX();
			state=STATE_START;
			*/
			break;
			
		case MotionEvent.ACTION_UP:
			/*
			status_txt.setText("Action up:"+event.getX()+" "+event.getY());
			end = event.getX();
			distance = MeasureUtil.pixToMillimeter(end-start);
			distance_txt.setText(String.format("%6.2f", distance)+"mm");
			*/
			if(state==STATE_MEASURE){
				end = current_x;
				state=STATE_END;
				distance = MeasureUtil.pixToMillimeter(end-start);
				distance_txt.setText(String.format("%6.2f", distance)+"mm");
				status_txt.setText("Measurement over");
			}
			if(state==STATE_START){
				state=STATE_END;
				
				distance_txt.setText(String.format("%6.2f", 0.0f)+"mm");
				status_txt.setText("Touch up");
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if(prev_timeStamp!=0&&prev_x!=0){
				float speed = Math.abs((current_x - prev_x)/(current_timeStamp-prev_timeStamp));
				Log.d("speed threshold","speed:"+speed);
				if(state!=STATE_END){
					
				}
				if(state==STATE_START && speed>THRESHOLD_SPEED){
					mTestView.setStart_X(current_x);
					mTestView.startMeasuring();
					start = current_x;
					state=STATE_MEASURE;
					
					status_txt.setText("Measuring...");
					takePicture_Threshold = 0;
					//takePicture();
				}
				if(state==STATE_MEASURE && speed<THRESHOLD_SPEED){
					
					
					distance = MeasureUtil.pixToMillimeter(end-start);
					distance_txt.setText(String.format("%6.2f", distance)+"mm");
					if(distance>3){
					state=STATE_END;
					status_txt.setText("Measurement over");
					}
					
				}
				if(state==STATE_MEASURE){
					end = current_x;
					mTestView.setEnd_X(current_x);
					distance = MeasureUtil.pixToMillimeter(end - start);
					
					distance_txt.setText(String.format("%6.2f", distance)+"mm");
				}
			}
			/*
			end = event.getX();
			distance = MeasureUtil.pixToMillimeter(end-start);
			distance_txt.setText(String.format("%6.2f", distance)+"mm");
			status_txt.setText("Action move:"+event.getX()+" "+event.getY());
			*/
			break;
			
		default:
			break;
		}
		mTestView.invalidate();
		prev_timeStamp = current_timeStamp;
		prev_x = current_x;
		
	}
/*
	PictureCallback mJpegCallBack = new PictureCallback() {
		
		@Override
		public void onPictureTaken(byte[] arg0, Camera arg1) {
			// TODO Auto-generated method stub
			
		}
	};
	*/
	/*
	private void takePicture() {
		
		mCamera.takePicture(null, null, mJpegCallBack);
		
	}
*/
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
			int defaultCameraId = 0;
            // Find the ID of the default camera
            CameraInfo cameraInfo = new CameraInfo();
                for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                    Camera.getCameraInfo(i, cameraInfo);
                    if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
                        defaultCameraId = i;
//                      Toast.makeText(getApplicationContext(), "front", Toast.LENGTH_LONG).show();
                    }/*
                    else if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                        defaultCameraId = i;
//                      Toast.makeText(getApplicationContext(), ""+defaultCameraId, Toast.LENGTH_LONG).show();
                    }*/
                }
    

		mCamera = null;
		try{
			mCamera = Camera.open(defaultCameraId);
		} catch (Exception e){
			
		}
		mCamera.setPreviewCallback(this);
		mCamera.setDisplayOrientation(0);
		Camera.Parameters params= mCamera.getParameters();
		/*
        List<Size> sizes = params.getSupportedPreviewSizes();
        Size mSize = sizes.get(0);
        mSurfaceView.getLayoutParams().width=mSize.width;  //params.getPreviewSize().width;
		mSurfaceView.getLayoutParams().height=mSize.height;  //params.getPreviewSize().height;
		Log.d("Ruler","Preview Sizes:"+mSize.width+" : "+mSize.height);
        params.setPreviewSize(mSize.width,mSize.height);
        mCamera.setParameters(params);
        
        sizes = params.getSupportedPictureSizes();
        mSize = sizes.get(0);
        Log.d("Ruler","Picture Sizes:"+mSize.width+" : "+mSize.height);
        */
		List<Size> sizes = params.getSupportedPreviewSizes();
		Size optimalSize = getOptimalPreviewSize(sizes, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		params.setPreviewSize(optimalSize.width, optimalSize.height);
		mCamera.setParameters(params);
		
		try{
			mCamera.setPreviewDisplay(mSurfaceHolder);
		} catch(Exception e){
			
		}
		mCamera.startPreview();
		
	}

	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
	    final double ASPECT_TOLERANCE = 0.05;
	    double targetRatio = (double) w/h;

	    if (sizes==null) return null;

	    Size optimalSize = null;

	    double minDiff = Double.MAX_VALUE;

	    int targetHeight = h;

	    // Find size
	    for (Size size : sizes) {
	        double ratio = (double) size.width / size.height;
	        if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
	        if (Math.abs(size.height - targetHeight) < minDiff) {
	            optimalSize = size;
	            minDiff = Math.abs(size.height - targetHeight);
	        }
	    }

	    if (optimalSize == null) {
	        minDiff = Double.MAX_VALUE;
	        for (Size size : sizes) {
	            if (Math.abs(size.height - targetHeight) < minDiff) {
	                optimalSize = size;
	                minDiff = Math.abs(size.height - targetHeight);
	            }
	        }
	    }
	    return optimalSize;
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		
		mSurfaceHolder.removeCallback(this);
		if(mCamera!=null){
			mCamera.stopPreview();
		}
		releaseCamera();
		
		
	}
    @Override
	protected void onResume() {
		mSurfaceHolder.addCallback(this);
		super.onResume();
	}

	private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
        
    }

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		if(state==STATE_MEASURE && distance >= takePicture_Threshold){
			takePicture_Threshold+=MEASURE_STEP;
			Log.d("capture image", "Enter");
			//takePicture(data,camera);
			//new SaveImage().execute(data,camera);
			new SaveImage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,data,camera,new Float(distance));
		}
		
	}
	
	private void takePicture(byte[] data, Camera camera, Float distance) {
		try {
			String timeStamp = (new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()));
			
	        Camera.Parameters parameters = camera.getParameters();
	        Size size = parameters.getPreviewSize();
	        YuvImage image = new YuvImage(data, parameters.getPreviewFormat(),
	                size.width, size.height, null);
	        File file = new File(Environment.getExternalStorageDirectory()
	                .getAbsolutePath() + SAVING_PATH +timeStamp+ String.format("%6.2f",distance.floatValue()) + ".jpg");
	        FileOutputStream filecon = new FileOutputStream(file);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        
	        image.compressToJpeg(
	                new Rect(0, 0, image.getWidth(), image.getHeight()), 90,
	                baos);
	        filecon.write(baos.toByteArray());
	        filecon.close();
	    } catch (FileNotFoundException e) {
	        Toast toast = Toast
	                .makeText(getBaseContext(), e.getMessage(), 1000);
	        toast.show();
	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private class SaveImage extends AsyncTask<Object, Void, Void>{



		@Override
		protected Void doInBackground(Object... params) {
			byte[] data = (byte[]) params[0];
			Camera camera = (Camera) params[1];
			Float distance = (Float) params[2];
			takePicture(data,camera,distance);
			return null;
		}
	}
}
