package com.example.ruler;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CalibrateActivity extends Activity implements OnClickListener{
	EditText realDistance_ET;
	Button calibrate_button;
	TextView pixDistance_TXT;
	ImageView first_bar_IV;
	ImageView second_bar_IV;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.calibrate);
		
		findAllViews();
	}

	private void findAllViews() {
		realDistance_ET = (EditText) findViewById(R.id.calibrate_distance_et);
		calibrate_button = (Button) findViewById(R.id.calibrate_calibrate_bt);
		pixDistance_TXT = (TextView) findViewById(R.id.calibrate_pix_distance_txt);
		first_bar_IV = (ImageView) findViewById(R.id.calibrate_1st_bar);
		second_bar_IV = (ImageView) findViewById(R.id.calibrate_2nd_bar);
		calibrate_button.setOnClickListener(this);
		
		
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		int first_left = first_bar_IV.getLeft();
		int second_left = second_bar_IV.getLeft();
		
		MeasureUtil.setPixDistance(Math.abs(second_left-first_left));
		pixDistance_TXT.setText(MeasureUtil.calibrate_Distance+"mm");
		super.onWindowFocusChanged(hasFocus);
	}

	@Override
	public void onClick(View arg0) {
		if(arg0==calibrate_button){
			//pixDistance_TXT.setText(realDistance_ET.getText().toString()+" mm");
			float distance = Float.parseFloat(realDistance_ET.getText().toString());
			MeasureUtil.setRealDistance(distance);
			pixDistance_TXT.setText(MeasureUtil.calibrate_Distance+"mm");
			savePreference();
		}
		
	}

	private void savePreference() {
		SharedPreferences.Editor editor = getSharedPreferences(MeasureUtil.SHARED_PREFERENCE_FILE, MODE_PRIVATE).edit();
		editor.putFloat(MeasureUtil.KEY_REAL_DISTANCE, Float.parseFloat(realDistance_ET.getText().toString()));
		editor.commit();
		Log.d("s_p","Save preference:"+Float.parseFloat(realDistance_ET.getText().toString()));
	}

}
