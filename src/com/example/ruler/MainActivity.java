package com.example.ruler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;



import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.SettingInjectorService;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	Button btn_main_measure;
	Button btn_main_calibrate;
	private String TAG_STACK_TRACE = "ruler_stack_trace";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);
		findAllViews();
	}


	@Override
	protected void onResume() {
		loadPreference();
		super.onResume();
	}


	private void loadPreference() {
		SharedPreferences prefs = getSharedPreferences(MeasureUtil.SHARED_PREFERENCE_FILE,MODE_PRIVATE);
		
		MeasureUtil.calibrate_Distance = prefs.getFloat(MeasureUtil.KEY_REAL_DISTANCE, MeasureUtil.calibrate_Distance);
		if(prefs.contains(MeasureUtil.KEY_REAL_DISTANCE)){
			Log.d("s_p","Load preference:"+prefs.getFloat(MeasureUtil.KEY_REAL_DISTANCE, MeasureUtil.calibrate_Distance));
		} else {
			Log.d("s_p","WHYYYYYYYYYYYYYYYYYYYYYYYYYYYYY!?");
		}
	}


	private void findAllViews() {
		
		btn_main_calibrate = (Button) findViewById(R.id.main_calibrate_btn);
		btn_main_measure = (Button) findViewById(R.id.main_measure_btn);
		
		btn_main_calibrate.setOnClickListener(mOnClickListener);
		btn_main_measure.setOnClickListener(mOnClickListener);
		
		
	}
	private OnClickListener mOnClickListener = new OnClickListener(){

		@Override
		public void onClick(View arg0) {
			if(arg0 == btn_main_calibrate){
				startActivity(new Intent(MainActivity.this, CalibrateActivity.class));
			}
			if(arg0 == btn_main_measure){
				startActivity(new Intent(MainActivity.this, MeasureActivity.class));
			}
			
		}
		
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
