A trivial app that can measure the horizontal distance that the finger swipes on the screen.

Need to calibrate using a real ruler first.

It can also take a video using the front camera during the swiping.